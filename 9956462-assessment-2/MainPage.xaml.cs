﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _9956462_assessment_2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        
        public MainPage()
        {
            this.InitializeComponent();
            var list = new List<string>();
            list.Add("Urgent");
            list.Add("Important");
            list.Add("Normal");
            list.Add("No Hurry");
            list.Add("...Meh");

            SubjectPriority.ItemsSource = list;
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            EmailFrom.Text = "";
            EmailMessage.Text = "";
            EmailTo.Text = "";
            SubjectPriority.Items.Clear();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }

        private async void Send_Click(object sender, RoutedEventArgs e)
        {

            var alertBox = new Windows.UI.Popups.MessageDialog(
               "Send To: " + EmailTo.Text + "\nSent From: " + EmailFrom.Text + 
               "\nSubject: " + SubjectPriority.SelectedItem + "\n\nMessage\n" + EmailMessage.Text);
            alertBox.Title = "Message";

            alertBox.Commands.Clear();
            alertBox.Commands.Add(new UICommand { Label = "Send", Id = 0 });
            alertBox.Commands.Add(new UICommand { Label = "Reset", Id = 1 });
            alertBox.Commands.Add(new UICommand { Label = "Cancel", Id = 2 });

            alertBox.DefaultCommandIndex = 0;
            alertBox.CancelCommandIndex = 2;

            var res = await alertBox.ShowAsync();

            if((int)res.Id == 0)
            {
                MessageDialog sent = new MessageDialog("Your message has been successfully sent");
                await sent.ShowAsync();
            }

            if((int)res.Id == 1)
            {
                EmailFrom.Text = "";
                EmailTo.Text = "";
                EmailMessage.Text = "";
                SubjectPriority.SelectedValue = null;
            }

        }
    }
}
